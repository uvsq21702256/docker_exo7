﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uvsq.tod.springtodo;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.ArrayList;
/**
 *
 * @author Seif
 */
@RestController
public class TacheUse {
    private static final String reponse = "Nom de la tache : %s.";
    private final AtomicLong compteurTaches = new AtomicLong();
    
    private List<Tache> Taches = new ArrayList<Tache>();
	
	
	
	//Q4 : récupération de la liste des taches
	@RequestMapping("/Afficher")
    public List<Tache> afficherTaches()
    {
		System.out.println("La liste des taches");
                Tache t1=new Tache(1,"seif");
                Tache t12=new Tache(2,"seif2");
                Taches.add(t1);
                Taches.add(t12);
		return Taches;
	}
	//Q5 : ajout d'une tache
    @RequestMapping(method = RequestMethod.POST)
    public void nouvelleTache(@RequestParam(value="nom", defaultValue="sans nom") String nom) 
    {
        Taches.add( new Tache(compteurTaches.incrementAndGet(), nom) );
        
        System.out.println("Tache ajoutée");
    }
    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }