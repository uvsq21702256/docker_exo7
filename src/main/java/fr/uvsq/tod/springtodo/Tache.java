/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uvsq.tod.springtodo;

/**
 *
 * @author Seif
 */
public class Tache {

    private final long num;
    private final String nom;

    public Tache(long num, String nom) {
        this.num = num;
        this.nom = nom;
    }

    public long getNumero() {
        return num;
    }

    public String getTitre() {
        return nom;
    }
}